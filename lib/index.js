/**
 * Module dependencies.
 */
var fs = require('fs')
  , path = require('path')
  , Server = require('./server');


/**
 * Create an OAuth 2.0 server.
 *
 * @return {Server}
 * @api public
 */
function createServer(options) {
  var server = new Server(options);
  return server;
}

// expose createServer() as the module
exports = module.exports = createServer;

/**
 * Export `.createServer()`.
 */
exports.createServer = createServer;


/**
 * Export middleware.
 */
exports.errorHandler = require('./middleware/errorHandler');

/**
 * Auto-load bundled grants.
 */
exports.grant = {};
 
var load = function () { return require('./grant/code'); };
exports.grant.__defineGetter__("code", load);
var load = function () { return require('./grant/token'); };
exports.grant.__defineGetter__("token", load);

// alias grants
exports.grant.authorizationCode = exports.grant.code;
exports.grant.implicit = exports.grant.token;

/**
 * Auto-load bundled exchanges.
 */
exports.exchange = {};

var load = function () { return require('./exchange/authorizationCode'); };
exports.exchange.__defineGetter__("authorizationCode", load);
var load = function () { return require('./exchange/clientCredentials'); };
exports.exchange.__defineGetter__("clientCredentials", load);
var load = function () { return require('./exchange/password'); };
exports.exchange.__defineGetter__("password", load);
var load = function () { return require('./exchange/refreshToken'); };
exports.exchange.__defineGetter__("refreshToken", load);

// alias exchanges
exports.exchange.code = exports.exchange.authorizationCode;

/**
 * Export errors.
 */
exports.OAuth2Error = require('./errors/oauth2error');
exports.AuthorizationError = require('./errors/authorizationerror');
exports.TokenError = require('./errors/tokenerror');
